import type { Signer } from '@ethersproject/abstract-signer'
import type { Provider } from '@ethersproject/providers'
import { Contract } from '@ethersproject/contracts'
import { simpleRpcProvider } from 'utils/providers'
import poolsConfig from 'config/constants/pools'
import { PoolCategory } from 'config/constants/types'
import tokens from 'config/constants/tokens'

// Addresses
import {
  getAddress,
  getChiefFarmerAddress,
  getTaskMasterAddress,
  getWayaVaultAddress,
  getMulticallAddress,
} from 'utils/addressHelpers'

// ABI
import bep20Abi from 'config/abi/erc20.json'
import erc721Abi from 'config/abi/erc721.json'
import lpTokenAbi from 'config/abi/lpToken.json'
import wayaAbi from 'config/abi/waya.json'
import chiefFarmer from 'config/abi/ChiefFarmer.json'
import taskMasterV1 from 'config/abi/TaskMaster.json'
import taskAssistant from 'config/abi/TaskAssistant.json'
import taskAssistantV2 from 'config/abi/TaskAssistantV2.json'
import taskAssistantBnb from 'config/abi/TaskAssistantBnb.json'
import wayaVaultAbi from 'config/abi/wayaVault.json'
import MultiCallAbi from 'config/abi/Multicall.json'
import erc721CollectionAbi from 'config/abi/erc721collection.json'

// Types
import type {
  Erc20,
  Erc721,
  Waya,
  ChiefFarmer,
  TaskMaster,
  TaskAssistant,
  TaskAssistantV2,
  LpToken,
  Erc721collection,
  WayaVault,
  Multicall,
} from 'config/abi/types'

export const getContract = (abi: any, address: string, signer?: Signer | Provider) => {
  const signerOrProvider = signer ?? simpleRpcProvider
  return new Contract(address, abi, signerOrProvider)
}

export const getBep20Contract = (address: string, signer?: Signer | Provider) => {
  return getContract(bep20Abi, address, signer) as Erc20
}
export const getErc721Contract = (address: string, signer?: Signer | Provider) => {
  return getContract(erc721Abi, address, signer) as Erc721
}
export const getLpContract = (address: string, signer?: Signer | Provider) => {
  return getContract(lpTokenAbi, address, signer) as LpToken
}
export const getTaskassistantContract = (id: number, signer?: Signer | Provider) => {
  const config = poolsConfig.find((pool) => pool.sousId === id)
  const abi = config.poolCategory === PoolCategory.BINANCE ? taskAssistantBnb : taskAssistant
  return getContract(abi, getAddress(config.contractAddress), signer) as TaskAssistant
}
export const getTaskassistantV2Contract = (id: number, signer?: Signer | Provider) => {
  const config = poolsConfig.find((pool) => pool.sousId === id)
  return getContract(taskAssistantV2, getAddress(config.contractAddress), signer) as TaskAssistantV2
}

export const getWayaContract = (signer?: Signer | Provider) => {
  return getContract(wayaAbi, tokens.waya.address, signer) as Waya
}
export const getChieffarmerContract = (signer?: Signer | Provider) => {
  return getContract(chiefFarmer, getChiefFarmerAddress(), signer) as ChiefFarmer
}
export const getTaskmasterContract = (signer?: Signer | Provider) => {
  return getContract(taskMasterV1, getTaskMasterAddress(), signer) as TaskMaster
}

export const getWayaVaultContract = (signer?: Signer | Provider) => {
  return getContract(wayaVaultAbi, getWayaVaultAddress(), signer) as WayaVault
}

export const getErc721CollectionContract = (signer?: Signer | Provider, address?: string) => {
  return getContract(erc721CollectionAbi, address, signer) as Erc721collection
}

export const getMulticallContract = () => {
  return getContract(MultiCallAbi, getMulticallAddress(), simpleRpcProvider) as Multicall
}
