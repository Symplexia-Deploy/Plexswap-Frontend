import { TranslateFunction } from 'contexts/Localization/types'
import { SalesSectionProps } from '.'

export const swapSectionData = (t: TranslateFunction): SalesSectionProps => ({
  headingText: t('Trade anything. No registration, no hassle.'),
  bodyText: t('Trade any token on BNB Smart Chain in seconds, just by connecting your wallet.'),
  reverse: false,
  primaryButton: {
    to: '/swap',
    text: t('Trade Now'),
    external: false,
  },
  images: {
    path: '/images/home/trade/',
    attributes: [{ src: 'Coins_SMPX', alt: t('SMPX token') }],
  },
})

export const wayaSectionData = (t: TranslateFunction): SalesSectionProps => ({
  headingText: t('WAYA makes our world go round.'),
  bodyText: t(
    'WAYA token is at the heart of the PlexSwap ecosystem. Buy it, win it, farm it, spend it, stake it... heck, you can even vote with it!',
  ),
  reverse: false,
  primaryButton: {
    to: '/swap?outputCurrency=0x0e09fabb73bd3ade0a17ecc321fd13a19e81ce82',
    text: t('Buy WAYA'),
    external: false,
  },
  images: {
    path: '/images/home/waya/',
    attributes: [{ src: 'Waya', alt: t('WAYA token') }],
  },
})
