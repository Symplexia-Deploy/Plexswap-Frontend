import IFOImage from './IFO.png'
import IFOMobileImage from './IFOMobile.png'
import competitionImage from './competition.png'
import competitionMobileImage from './competitionMobile.png'
import modImage from './mod-tc-desktop.png'
import modMobileImage from './mod-tc-mobile.png'
import modWhiteLogo from './mod-white-logo.png'
import perpetualImage from './perpetual.png'
import perpetualMobileImage from './perpetualMobile.png'

export {
  IFOImage,
  competitionImage,
  modImage,
  modMobileImage,
  competitionMobileImage,
  IFOMobileImage,
  modWhiteLogo,
  perpetualImage,
  perpetualMobileImage,
}
