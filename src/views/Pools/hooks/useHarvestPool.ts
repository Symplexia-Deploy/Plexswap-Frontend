import { useCallback } from 'react'
import { BIG_ZERO } from 'utils/bigNumber'
import getGasPrice from 'utils/getGasPrice'
import { useTaskAssistant } from 'hooks/useContract'
import { DEFAULT_GAS_LIMIT } from 'config'

const options = {
  gasLimit: DEFAULT_GAS_LIMIT,
}

const harvestPool = async (taskAssistantContract) => {
  const gasPrice = getGasPrice()
  return taskAssistantContract.deposit('0', { ...options, gasPrice })
}

const harvestPoolBnb = async (taskAssistantContract) => {
  const gasPrice = getGasPrice()
  return taskAssistantContract.deposit({ ...options, value: BIG_ZERO, gasPrice })
}

const useHarvestPool = (sousId, isUsingBnb = false) => {
  const taskAssistantContract = useTaskAssistant(sousId)

  const handleHarvest = useCallback(async () => {
    if (isUsingBnb) {
      return harvestPoolBnb(taskAssistantContract)
    }

    return harvestPool(taskAssistantContract)
  }, [isUsingBnb, taskAssistantContract])

  return { onReward: handleHarvest }
}

export default useHarvestPool
