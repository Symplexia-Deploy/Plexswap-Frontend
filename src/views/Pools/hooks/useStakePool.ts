import { useCallback } from 'react'
import BigNumber from 'bignumber.js'
import { DEFAULT_TOKEN_DECIMAL, DEFAULT_GAS_LIMIT } from 'config'
import { BIG_TEN } from 'utils/bigNumber'
import { useTaskAssistant } from 'hooks/useContract'
import getGasPrice from 'utils/getGasPrice'

const options = {
  gasLimit: DEFAULT_GAS_LIMIT,
}

const sousStake = async (taskAssistantContract, amount, decimals = 18) => {
  const gasPrice = getGasPrice()
  return taskAssistantContract.deposit(new BigNumber(amount).times(BIG_TEN.pow(decimals)).toString(), {
    ...options,
    gasPrice,
  })
}

const sousStakeBnb = async (taskAssistantContract, amount) => {
  const gasPrice = getGasPrice()
  return taskAssistantContract.deposit(new BigNumber(amount).times(DEFAULT_TOKEN_DECIMAL).toString(), {
    ...options,
    gasPrice,
  })
}

const useStakePool = (sousId: number, isUsingBnb = false) => {
  const taskAssistantContract = useTaskAssistant(sousId)

  const handleStake = useCallback(
    async (amount: string, decimals: number) => {
      if (isUsingBnb) {
        return sousStakeBnb(taskAssistantContract, amount)
      }
      return sousStake(taskAssistantContract, amount, decimals)
    },
    [isUsingBnb, taskAssistantContract],
  )

  return { onStake: handleStake }
}

export default useStakePool
