import { Flex, Text } from '@plexswap/ui-plex'
import { useWeb3React } from '@web3-react/core'
import { useTranslation } from 'contexts/Localization'
import { usePriceWayaBusd } from 'state/farms/hooks'
import { useVaultPoolByKey } from 'state/pools/hooks'
import { DeserializedPool } from 'state/types'
import { getWayaVaultEarnings } from 'views/Pools/helpers'
import RecentWayaProfitBalance from './RecentWayaProfitBalance'

const RecentWayaProfitCountdownRow = ({ pool }: { pool: DeserializedPool }) => {
  const { t } = useTranslation()
  const { account } = useWeb3React()
  const {
    pricePerFullShare,
    userData: { wayaAtLastUserAction, userShares, currentOverdueFee, currentPerformanceFee },
  } = useVaultPoolByKey(pool.vaultKey)
  const wayaPriceBusd = usePriceWayaBusd()
  const { hasAutoEarnings, autoWayaToDisplay } = getWayaVaultEarnings(
    account,
    wayaAtLastUserAction,
    userShares,
    pricePerFullShare,
    wayaPriceBusd.toNumber(),
    currentPerformanceFee.plus(currentOverdueFee),
  )

  if (!(userShares.gt(0) && account)) {
    return null
  }

  return (
    <Flex alignItems="center" justifyContent="space-between">
      <Text fontSize="14px">{`${t('Recent WAYA profit')}:`}</Text>
      {hasAutoEarnings && <RecentWayaProfitBalance wayaToDisplay={autoWayaToDisplay} pool={pool} account={account} />}
    </Flex>
  )
}

export default RecentWayaProfitCountdownRow
