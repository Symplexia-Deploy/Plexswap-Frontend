import { useWeb3React } from '@web3-react/core'
import { FetchStatus } from 'config/constants/types'
import useSWRImmutable from 'swr/immutable'
import { getAddress } from 'utils/addressHelpers'
import { getActivePools } from 'utils/calls'
import { simpleRpcProvider } from 'utils/providers'
import { getVotingPower } from '../helpers'

interface State {
  wayaBalance?: number
  wayaVaultBalance?: number
  wayaPoolBalance?: number
  poolsBalance?: number
  wayaBnbLpBalance?: number
  total: number
}

const useGetVotingPower = (block?: number, isActive = true): State & { isLoading: boolean; isError: boolean } => {
  const { account } = useWeb3React()
  const { data, status, error } = useSWRImmutable(
    account && isActive ? [account, block, 'votingPower'] : null,
    async () => {
      const blockNumber = block || (await simpleRpcProvider.getBlockNumber())
      const eligiblePools = await getActivePools(blockNumber)
      const poolAddresses = eligiblePools.map(({ contractAddress }) => getAddress(contractAddress))
      const { wayaBalance, wayaBnbLpBalance, wayaPoolBalance, total, poolsBalance, wayaVaultBalance } =
        await getVotingPower(account, poolAddresses, blockNumber)
      return {
        wayaBalance,
        wayaBnbLpBalance,
        wayaPoolBalance,
        poolsBalance,
        wayaVaultBalance,
        total,
      }
    },
  )
  if (error) console.error(error)

  return { ...data, isLoading: status !== FetchStatus.Fetched, isError: status === FetchStatus.Failed }
}

export default useGetVotingPower
