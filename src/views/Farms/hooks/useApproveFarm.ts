import { useCallback } from 'react'
import { MaxUint256 } from '@ethersproject/constants'
import { Contract } from '@ethersproject/contracts'
import { useChieffarmer } from 'hooks/useContract'
import { useCallWithGasPrice } from 'hooks/useCallWithGasPrice'

const useApproveFarm = (lpContract: Contract) => {
  const chiefFarmerContract = useChieffarmer()
  const { callWithGasPrice } = useCallWithGasPrice()
  const handleApprove = useCallback(async () => {
    return callWithGasPrice(lpContract, 'approve', [chiefFarmerContract.address, MaxUint256])
  }, [lpContract, chiefFarmerContract, callWithGasPrice])

  return { onApprove: handleApprove }
}

export default useApproveFarm
