import chieffarmerABIV1 from 'config/abi/ChiefFarmer.json'
import chunk from 'lodash/chunk'
import { multicallv2 } from 'utils/multicall'
import { SerializedFarmConfig } from '../../config/constants/types'
import { SerializedFarm } from '../types'
import { getTaskMasterAddress } from '../../utils/addressHelpers'
import { getTaskmasterContract } from '../../utils/contractHelpers'

const chiefFarmerAddress = getTaskMasterAddress()
const chiefFarmerContract = getTaskmasterContract()

export const fetchChiefFarmerFarmPoolLength = async () => {
  const poolLength = await chiefFarmerContract.poolLength()
  return poolLength
}

const chiefFarmerFarmCalls = (farm: SerializedFarm) => {
  const { pidTM } = farm
  return pidTM || pidTM === 0
    ? [
        {
          address: chiefFarmerAddress,
          name: 'poolInfo',
          params: [pidTM],
        },
        {
          address: chiefFarmerAddress,
          name: 'totalAllocPoint',
        },
      ]
    : [null, null]
}

export const fetchChiefFarmerData = async (farms: SerializedFarmConfig[]): Promise<any[]> => {
  const chiefFarmerCalls = farms.map((farm) => chiefFarmerFarmCalls(farm))
  const chunkSize = chiefFarmerCalls.flat().length / farms.length
  const chiefFarmerAggregatedCalls = chiefFarmerCalls
    .filter((chiefFarmerCall) => chiefFarmerCall[0] !== null && chiefFarmerCall[1] !== null)
    .flat()
  const chiefFarmerMultiCallResult = await multicallv2(chieffarmerABIV1, chiefFarmerAggregatedCalls)
  const chiefFarmerChunkedResultRaw = chunk(chiefFarmerMultiCallResult, chunkSize)
  let chiefFarmerChunkedResultCounter = 0
  return chiefFarmerCalls.map((chiefFarmerCall) => {
    if (chiefFarmerCall[0] === null && chiefFarmerCall[1] === null) {
      return [null, null]
    }
    const data = chiefFarmerChunkedResultRaw[chiefFarmerChunkedResultCounter]
    chiefFarmerChunkedResultCounter++
    return data
  })
}
