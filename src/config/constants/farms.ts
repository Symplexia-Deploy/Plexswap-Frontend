import { serializeTokens } from './tokens'
import { SerializedFarmConfig } from './types'
import { CHAIN_ID } from './networks'
import { pidTMBusdBnb } from './plexInfo'

const serializedTokens = serializeTokens()

const farms: SerializedFarmConfig[] = [
  /**
   * These 3 farms (PID 0, 1, 2) should always be at the top of the file.
   */
  {
    pid: 0,
    pidTM: 0,
    lpSymbol: 'WAYA',
    lpAddresses: {
      97: '0x156644aF8F57BD264cedeF94e9c410DA2BB19B26', // WAYA Address
      56: '0x0E09FaBB73Bd3Ade0a17ECC321fD13a19e81cE82',
    },
    token: serializedTokens.gaya,
    quoteToken: serializedTokens.wbnb,
  },
  {
    pid: 1,
    pidTM: 2,
    lpSymbol: 'WAYA-BNB LP',
    lpAddresses: {
      97: '0x05b5bCc9F207E013916Ed3345932F2d736D8653e', // PLEX-LP Pair
      56: '0x0eD7e52944161450477ee417DE9Cd3a859b14fD0',
    },
    token: serializedTokens.waya,
    quoteToken: serializedTokens.wbnb,
  },
  {
    pid: 2,
    pidTM: 3,
    lpSymbol: 'PLEX-BNB LP',
    lpAddresses: {
      97: '0x22aE89104C0A2a0792568b8CDf5A7806249d6e90', // PLEX-LP Pair
      56: '',
    },
    token: serializedTokens.plex,
    quoteToken: serializedTokens.wbnb,
  },
  {
    pid: 3,
    pidTM: pidTMBusdBnb, // (**LOOKOUT**)
    lpSymbol: 'BUSD-BNB LP',
    lpAddresses: {
      97: '0xC29d9C4Cc6ee9f25A486bdd74D6f6aC60068cffE',
      56: '0x58F876857a02D6762E0101bb5C46A8c1ED44Dc16',
    },
    token: serializedTokens.busd,
    quoteToken: serializedTokens.wbnb,
  },
  // * V3 by order of release (some may be out of PID order due to multiplier boost)
  {
    pid: 4,
    pidTM: 6,
    lpSymbol: 'WAYA-BUSD LP',
    lpAddresses: {
      97: '0x6d4F8b266809A3B1feF3Ce257fFCdFDE5fEDed3d',
      56: '0x804678fa97d91B974ec2af3c843270886528a9E6',
    },
    token: serializedTokens.waya,
    quoteToken: serializedTokens.busd,
  },
].filter((f) => !!f.lpAddresses[CHAIN_ID])

export default farms
