import { ContextApi } from 'contexts/Localization/types'
import { PageMeta } from './types'

export const DEFAULT_META: PageMeta = {
  title: 'PlexSwap',
  description:
    'The most popular AMM on BSC by user count! Earn WAYA through yield farming, then stake it in Gaya Pools to earn more tokens! Initial Farm Offerings (new token launch model pioneered by PlexSwap), NFTs, and more, on a platform you can trust.',
  image: 'https://swap.plexfinance.us/images/hero.png',
}

export const getCustomMeta = (path: string, t: ContextApi['t']): PageMeta => {
  let basePath
  if (path.startsWith('/swap')) {
    basePath = '/swap'
  } else if (path.startsWith('/add')) {
    basePath = '/add'
  } else if (path.startsWith('/remove')) {
    basePath = '/remove'
  } else if (path.startsWith('/voting/proposal') && path !== '/voting/proposal/create') {
    basePath = '/voting/proposal'
  } else {
    basePath = path
  }

  switch (basePath) {
    case '/':
      return {
        title: `${t('Home')} | ${t('PlexSwap')}`,
      }
    case '/swap':
      return {
        title: `${t('Exchange')} | ${t('PlexSwap')}`,
      }
    case '/add':
      return {
        title: `${t('Add Liquidity')} | ${t('PlexSwap')}`,
      }
    case '/remove':
      return {
        title: `${t('Remove Liquidity')} | ${t('PlexSwap')}`,
      }
    case '/liquidity':
      return {
        title: `${t('Liquidity')} | ${t('PlexSwap')}`,
      }
    case '/find':
      return {
        title: `${t('Import Pool')} | ${t('PlexSwap')}`,
      }
    case '/farms':
      return {
        title: `${t('Farms')} | ${t('PlexSwap')}`,
      }
    case '/pools':
      return {
        title: `${t('Pools')} | ${t('PlexSwap')}`,
      }
    case '/voting':
      return {
        title: `${t('Voting')} | ${t('PlexSwap')}`,
      }
    case '/voting/proposal':
      return {
        title: `${t('Proposals')} | ${t('PlexSwap')}`,
      }
    case '/voting/proposal/create':
      return {
        title: `${t('Make a Proposal')} | ${t('PlexSwap')}`,
      }
    case '/info':
      return {
        title: `${t('Overview')} | ${t('PlexSwap Info & Analytics')}`,
        description: 'View statistics for Plexswap exchanges.',
      }
    case '/info/pools':
      return {
        title: `${t('Pools')} | ${t('PlexSwap Info & Analytics')}`,
        description: 'View statistics for Plexswap exchanges.',
      }
    case '/info/tokens':
      return {
        title: `${t('Tokens')} | ${t('PlexSwap Info & Analytics')}`,
        description: 'View statistics for Plexswap exchanges.',
      }
    default:
      return null
  }
}
