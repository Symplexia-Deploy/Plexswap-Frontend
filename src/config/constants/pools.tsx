import { BigNumber } from '@ethersproject/bignumber'
import Trans from 'components/Trans'
import { VaultKey } from 'state/types'
import { CHAIN_ID } from './networks'
import tokens, { serializeTokens } from './tokens'
import { SerializedPoolConfig, PoolCategory } from './types'

const serializedTokens = serializeTokens()

export const MAX_LOCK_DURATION = 31536000
export const UNLOCK_FREE_DURATION = 604800
export const ONE_WEEK_DEFAULT = 604800
export const BOOST_WEIGHT = BigNumber.from('20000000000000')
export const DURATION_FACTOR = BigNumber.from('31536000')

export const vaultPoolConfig = {
  [VaultKey.WayaVaultV1]: {
    name: <Trans>Auto WAYA</Trans>,
    description: <Trans>Automatic restaking</Trans>,
    autoCompoundFrequency: 5000,
    gasLimit: 380000,
    tokenImage: {
      primarySrc: `/images/tokens/${tokens.waya.address}.svg`,
      secondarySrc: '/images/tokens/autorenew.svg',
    },
  },
  [VaultKey.WayaVault]: {
    name: <Trans>Stake WAYA</Trans>,
    description: <Trans>Stake, Earn – And more!</Trans>,
    autoCompoundFrequency: 5000,
    gasLimit: 500000,
    tokenImage: {
      primarySrc: `/images/tokens/${tokens.waya.address}.svg`,
      secondarySrc: '/images/tokens/autorenew.svg',
    },
  },
} as const

const pools: SerializedPoolConfig[] = [
  {
    sousId: 0,
    stakingToken: serializedTokens.waya,
    earningToken: serializedTokens.waya,
    contractAddress: {
      97: '0x920cE1F6Bb3a34Feda8b3eeDA122d312F229787E',
      56: '0xa5f8C5Dbd5F286960b9d90548680aE5ebFf07652', // ChiefFarmer Address (**CHECKOUT**)
    },
    poolCategory: PoolCategory.CORE,
    harvest: true,
    tokenPerBlock: '10',
    sortOrder: 1,
    isFinished: false,
  },

  {
    sousId: 142,
    stakingToken: serializedTokens.doge,
    earningToken: serializedTokens.waya,
    contractAddress: {
      97: '',
      56: '0xbebd44824631b55991fa5f2bf5c7a4ec96ff805b', // SmartChefInitializable
    },
    poolCategory: PoolCategory.CORE,
    harvest: true,
    sortOrder: 999,
    tokenPerBlock: '0.01388',
    isFinished: false,
  },
  {
    sousId: 136,
    stakingToken: serializedTokens.waya,
    earningToken: serializedTokens.ust,
    contractAddress: {
      97: '',
      56: '0xce3ebac3f549ebf1a174a6ac3b390c179422b5f6',
    },
    poolCategory: PoolCategory.CORE,
    harvest: true,
    sortOrder: 999,
    tokenPerBlock: '0.17361',
    isFinished: false,
  },
].filter((p) => !!p.contractAddress[CHAIN_ID])

export default pools
