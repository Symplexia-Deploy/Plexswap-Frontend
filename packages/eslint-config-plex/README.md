# eslint-config-plex

Plex Eslint config with:

- Airbnb config
- Typescript
- Prettier

## Usage

```
npx install-peerdeps --dev @plexswap/eslint-config-plex
```

Add `"extends": "@plexswap/eslint-config-plex"` to your eslint config file.
