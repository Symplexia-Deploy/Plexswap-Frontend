# ![image](https://user-images.githubusercontent.com/106164850/170869887-1bfee9e7-c0f7-4480-a1ee-7ad96bec37f6.png) Plexswap Frontend

This project contains the main features of the plex application.

## Quick Start

install dependencies using **yarn**

```sh
yarn
```

start the development server
```sh
yarn dev
```

build with production mode
```sh
yarn build

# start the application after build
yarn start
```
